This design includes tables for users, posts, comments, likes, friend requests, and user authentication.

### Table: Users

| Field Name       | Data Type    | Validations                          |
| ---------------- | ------------ | ------------------------------------ |
| UserID           | INT          | Primary Key, Auto-increment           |
| UserName         | VARCHAR(50)  | Unique, Not Null                     |
| FirstName        | VARCHAR(50)  | Not Null                             |
| LastName         | VARCHAR(50)  | Not Null                             |
| Email            | VARCHAR(100) | Unique, Not Null, Email format        |
| PasswordHash     | VARCHAR(255) | Not Null                             |
| DateJoined       | DATETIME     | Default: Current Timestamp           |

### Table: Posts

| Field Name       | Data Type    | Validations                          |
| ---------------- | ------------ | ------------------------------------ |
| PostID           | INT          | Primary Key, Auto-increment           |
| UserID           | INT          | Foreign Key (Users), Not Null        |
| Content          | TEXT         | Not Null                             |
| Timestamp        | DATETIME     | Default: Current Timestamp           |

### Table: Comments

| Field Name       | Data Type    | Validations                          |
| ---------------- | ------------ | ------------------------------------ |
| CommentID        | INT          | Primary Key, Auto-increment           |
| PostID           | INT          | Foreign Key (Posts), Not Null         |
| UserID           | INT          | Foreign Key (Users), Not Null        |
| CommentText      | TEXT         | Not Null                             |
| Timestamp        | DATETIME     | Default: Current Timestamp           |

### Table: Likes

| Field Name       | Data Type    | Validations                          |
| ---------------- | ------------ | ------------------------------------ |
| LikeID           | INT          | Primary Key, Auto-increment           |
| PostID           | INT          | Foreign Key (Posts), Not Null         |
| UserID           | INT          | Foreign Key (Users), Not Null        |

### Table: FriendRequests

| Field Name       | Data Type    | Validations                          |
| ---------------- | ------------ | ------------------------------------ |
| RequestID        | INT          | Primary Key, Auto-increment           |
| SenderUserID     | INT          | Foreign Key (Users), Not Null        |
| ReceiverUserID   | INT          | Foreign Key (Users), Not Null        |
| Status           | VARCHAR(20)  | Pending, Accepted, Declined          |
| Timestamp        | DATETIME     | Default: Current Timestamp           |

### Table: UserSessions (for Authentication)

| Field Name       | Data Type    | Validations                          |
| ---------------- | ------------ | ------------------------------------ |
| SessionID        | VARCHAR(255) | Primary Key, Unique                  |
| UserID           | INT          | Foreign Key (Users), Not Null        |
| ExpiryTimestamp  | DATETIME     | Not Null                             |

### Associations:

- Each user can have multiple posts, comments, and likes, so there is a one-to-many relationship between Users and Posts, Comments, and Likes.
- Friend requests involve two users, so there is a many-to-many relationship between Users (SenderUserID and ReceiverUserID) in the FriendRequests table.

### Authentication and Authorization:

- User authentication is managed through the UserSessions table, where a session is created upon successful login.
- Authorization is enforced through foreign keys and appropriate checks on the server-side to ensure that users can only interact with their own data or data they have permission to access.
