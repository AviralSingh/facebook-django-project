**User Personas**

## Facebook

1. Consumer Persona

- Aviral
- Male 21
- Software Engineer
- Bored. Looking to timepass during free hours

Interests:
Looking for news regarding sports, health and following entertainers.

Follows:
- Cristiano Ronaldo
- Fabrizio Romano
- La Liga
- His friends

---
2. Creator Persona

- Fabrizio Romano
- Male 30
- Italian sports journalist
- Specializing in news about football transfers

Interest:
Football, Transfer Market and gaining  followers via providing fast and reliable news.

Follows:
- Other footballers
- Transfer journalists
