**User Stories:**

1. **As a non-logged-in user, I can view my Facebook feed.**

2. **As a non-logged-in user, I can search for and view user profiles.**

3. **As a non-logged-in user, I can sign up for a Facebook account.**
   - Start Execution: *Click on "Sign Up" button.*
   - UI: A registration form with fields for name, email, password, and other required information.
   - Actions: Input information, click on "Sign Up" button.

4. **As a non-logged-in user, I can log in to Facebook.**
   - Start Execution: *Click on "Log In" button.*
   - UI: A login form with fields for email/username and password.
   - Actions: Input credentials, click on "Log In" button.

5. **As a logged-in user, I can create a new text/status post.**
   - Start Execution: *Click on the "Create Post" button.*
   - UI: A text input box for composing the post, possibly with additional options like adding photos or location.
   - Actions: Type the post content, add optional elements, click on "Post" button.

6. **As a logged-in user, I can edit my own text/status post.**
   - Start Execution: *Click on the "Edit" option on my post.*
   - UI: The post content becomes editable, possibly with additional options for editing.
   - Actions: Modify the post content, click on "Save" or "Update" button.

7. **As a logged-in user, I can create and edit my profile page.**
   - Start Execution: *Navigate to the "Profile" section and click on "Edit Profile."*
   - UI: A form with fields for updating profile information, such as bio, work, education, etc.
   - Actions: Update relevant fields, click on "Save" or "Update" button.

8. **As a logged-in user, I can like other users' posts.**
   - Start Execution: *Click on the "Like" button on a post.*
   - UI: A "Like" icon or button next to each post.
   - Actions: Click on the "Like" button to express appreciation for the post.

9. **As a logged-in user, I can comment on all posts.**
   - Start Execution: *Click on the "Comment" button on a post.*
   - UI: A comment input box below each post.
   - Actions: Type the comment, click on "Post" button.

10. **As a logged-in user, I can comment on other comments (first level).**
    - Start Execution: *Click on the "Reply" button on a comment.*
    - UI: A reply input box below the comment.
    - Actions: Type the reply, click on "Post" button.

11. **As a logged-in user, I can save other users' posts.**
    - Start Execution: *Click on the "Save" button on a post.*
    - UI: A "Save" option next to each post.
    - Actions: Click on the "Save" button to bookmark the post for later viewing.

